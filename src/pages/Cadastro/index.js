import React, { useState } from 'react'
import firebase from '../../services/api'
import { toast, ToastContainer } from 'react-toastify'

export default function Cadastro() {
	const [email, setEmail] = useState('')
	const [senha, setSenha] = useState('')

	const handlechangeEmail = e => {
		setEmail(e.target.value)
	}

	const handlechangeSenha = e => {
		setSenha(e.target.value)
	}

	const handleSubmit = e => {
		e.preventDefault()
		firebase
			.auth()
			.createUserWithEmailAndPassword(email, senha)
			.catch(error => {
				if (error.code == 'auth/weak-password') {
					toast.error('Sua senha deve ter pelo menos 6 caracteres!', {
						position: toast.POSITION.BOTTOM_CENTER,
					})
				} else if (error.code == 'auth/email-already-in-use') {
					toast.error('Este email já possui cadastro!', {
						position: toast.POSITION.BOTTOM_CENTER,
					})
				}
			})
		cancelForm()
	}

	const cancelForm = () => {
		document.getElementById('cancel-form').reset()
	}

	return (
		<div>
			<div className='container-fluid p-3 d-flex justify-content-center'>
				<div className='card col-md-8 col-sm-12 p-3'>
					<form className='form-row' id='cancel-form' onSubmit={handleSubmit} id='cancel-form'>
						<div className='col-6 mb-3'>
							<label>Email:</label>
							<input className='form-control ' type='email' name='email' onChange={handlechangeEmail} required />
						</div>
						<div className='col-6 mb-3'>
							<label>Senha:</label>
							<input className='form-control' type='password' name='senha' onChange={handlechangeSenha} required />
						</div>
						<div className='col-6 mb-3'>
							<input className='btn btn-primary mr-2' type='submit' value='Entrar' />
						</div>

						<ToastContainer autoClose={3000} />
					</form>
				</div>
			</div>
		</div>
	)
}
