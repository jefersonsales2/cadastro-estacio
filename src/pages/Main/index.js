import React, { useEffect, useState } from 'react'
import Header from '../../components/Header'
import firebase from '../../services/api'
import Select from 'react-select'

import { toast, ToastContainer } from 'react-toastify'

export default function Main() {
	const [nome, setNome] = useState('')
	const [cpf, setCpf] = useState('')
	const [telefone, setTelefone] = useState('')
	const [email, setEmail] = useState('')
	const [cursos, setCursos] = useState([])
	const [unidades, setUnidades] = useState([])
	const [selectedOptionCursos, setSelectedOptionCursos] = useState(null)
	const [selectedOptionUnidades, setSelectedOptionUnidades] = useState(null)

	const handlechangeNome = e => {
		setNome(e.target.value)
	}

	const handlechangeCpf = e => {
		setCpf(e.target.value)
	}

	const handlechangeTelefone = e => {
		setTelefone(e.target.value)
	}

	const handlechangeEmail = e => {
		setEmail(e.target.value)
	}

	const handleChangeOptionsCusros = selectedOptionCursos => {
		setSelectedOptionCursos(selectedOptionCursos)
	}

	const handleChangeOptionsUnidades = selectedOptionUnidades => {
		setSelectedOptionUnidades(selectedOptionUnidades)
	}

	useEffect(() => {
		const dbCursos = firebase
			.firestore()
			.collection('cursos')
			.orderBy('description', 'asc')
		dbCursos.onSnapshot(snap => {
			const state = []
			snap.forEach(doc => {
				state.push({
					label: doc.data().description,
				})
			})
			setCursos(state)
		})
	}, [])

	useEffect(() => {
		const dbUnidades = firebase
			.firestore()
			.collection('unidades')
			.orderBy('description', 'asc')
		dbUnidades.onSnapshot(snap => {
			const state = []
			snap.forEach(doc => {
				state.push({
					label: doc.data().description,
				})
			})
			setUnidades(state)
		})
	}, [])

	const handleSubmit = e => {
		e.preventDefault()
		const db = firebase.firestore()
		db.collection('dados').add({
			nome: nome,
			cpf: cpf,
			telefone: telefone,
			email: email,
			cursoDesejado: selectedOptionCursos,
			unidade: selectedOptionUnidades,
		})
		toast.success('Cadastro efetuado com sucesso!', {
			position: toast.POSITION.BOTTOM_CENTER,
		})
		cancelForm()
		setSelectedOptionCursos(null)
		setSelectedOptionUnidades(null)
	}

	const cancelForm = () => {
		document.getElementById('cancel-form').reset()
	}

	return (
		<div>
			<Header name='Pagina de Cadastro' />

			<div className='container-fluid p-3 d-flex justify-content-center'>
				<div className='card col-md-8 col-sm-12 p-3'>
					<form className='form-row' onSubmit={handleSubmit} id='cancel-form'>
						<div className='col-12 mb-3'>
							<label>Nome:</label>
							<input className='form-control ' type='text' name='nome' onChange={handlechangeNome} required />
						</div>
						<div className='col-6 mb-3'>
							<label>CPF:</label>
							<input className='form-control' type='text' name='cpf' onChange={handlechangeCpf} required />
						</div>
						<div className='col-6  mb-3'>
							<label>Telefone:</label>
							<input className='form-control' type='text' name='telefone' onChange={handlechangeTelefone} required />
						</div>
						<div className='col-12 mb-3'>
							<label>Email:</label>
							<input className='form-control ' type='email' name='email' onChange={handlechangeEmail} required />
						</div>
						<div className='col-12 mb-3'>
							<label>Curso Desejado:</label>
							<Select value={selectedOptionCursos} onChange={handleChangeOptionsCusros} options={cursos} />
						</div>
						<div className='col-12 mb-3'>
							<label>Unidade:</label>
							<Select value={selectedOptionUnidades} onChange={handleChangeOptionsUnidades} options={unidades} />
						</div>
						<div className='col-6  mb-3'>
							<input className='btn btn-primary' type='submit' value='Enviar' />
						</div>
					</form>
					<ToastContainer autoClose={3000} />
				</div>
			</div>
		</div>
	)
}
