import firebase from 'firebase'
import 'firebase/firestore'

var firebaseConfig = {
	apiKey: 'AIzaSyASIiR8kBsLow3-jO9bhyMj6E7qe6ZdGZk',
	authDomain: 'cadastro-estacio.firebaseapp.com',
	databaseURL: 'https://cadastro-estacio.firebaseio.com',
	projectId: 'cadastro-estacio',
	storageBucket: 'cadastro-estacio.appspot.com',
	messagingSenderId: '773800616202',
	appId: '1:773800616202:web:b9e09c2672a36ce3871af3',
}

firebase.initializeApp(firebaseConfig)
export default firebase
