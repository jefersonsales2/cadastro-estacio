import React from 'react'
import Router from './router'
import 'semantic-ui-css/semantic.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-toastify/dist/ReactToastify.css'
function App() {
	return (
		<div className='App'>
			<Router />
		</div>
	)
}

export default App
