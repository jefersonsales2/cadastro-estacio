import React from 'react'

export default function Header(props) {
	return (
		<div>
			<nav className='navbar navbar-dark bg-primary'>
				<a className='navbar-brand' href='#'>
					<img className='mr-4 mb-1' src={require('../assets/Logo.png')} />
					{props.name}
				</a>
			</nav>
		</div>
	)
}
