import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Login from './pages/Login'
import Main from './pages/Main'
import Cadastro from './pages/Cadastro'

export default function Routes() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path='/' exact component={Login} />
				<Route path='/cadastro' exact component={Cadastro} />
				<Route path='/main' component={Main} />
			</Switch>
		</BrowserRouter>
	)
}
